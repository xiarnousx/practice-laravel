# Laravel 5 Cheat Sheet

Author : Ihab Arnous <iarnous@gmail.com>

Description : Preparing Cheat Sheet for SkillTest and for future reference

Resources Used For Preparation

1. [Quickstart Intermediate](https://laravel.com/docs/5.1/quickstart-intermediate) (Introductory) 
1. [Up Running Laravel](http://www.lynda.com/Laravel-tutorials/Up-Running-Laravel/) ( Waste of Time)
1. [Modern Web Development With Laravel](https://www.udemy.com/modern-web-development-with-laravel/) (Practical Hands-on)
1. [Laravel 5 Essentials](https://www.packtpub.com/web-development/laravel-5-essentials) (Excellent)

* * *

# Artisan Commands
```bash
php artisan make:migration x-table --table=|--create=x_tables
php artisan make:model 'Entities\Art\Paintings' [-m]
php artisan make:controller 'Arts\Art'
php artisan make:seeder UserTableSeeder
php artisan make:middleware TestBefore
php artisan make:provider CustomServiceProvider
php artisan make:request  CustomRequest

php artisan migrate
php artisan migrate:install
php artisan migrate:refresh [--seed]
php artisan migrate:rollback
php artisan migrate:reset

php artisan db:seed [--class=UsersTableSeeder]

php artisan event:generate

```

* * *
Remember, all Laravel app folders are auto-loaded using the 
PSR-4 auto-loading standard, so you are free to create as many extra 
directories as needed
* * *

# Blade

```html
{{ csrf_field() }}
@extends('layouts.app')
@yield('content')
@include('includes.nav')
@section('content')
@endsection
@include
@if @elsif @else @endif
@unless @endunless
@while @endwhile
@for @endfor
@foreach @endforeach
{{ csrf_field() }} | <input type="hidden" name="_token" value="{{ Sesstion::token()}}" />
{{ method_field('DELETE') }}
{{ URL::asset('assets/_scripts/respond.min.js') }}
{!! route('about.laravel',['v' => '1']) !!}
```

# Routes | Controllers | Validation

```php
<?php
// Post controller's method
public function index(Request $request)
{
    return view('tasks.index');
}

// If validation fails, method returns no further processing
$this->validate($request, [
        'name' => 'required|max:255',
        ...
    ]);

// All http verbs supported same format
Route::delete('/task/{task}', 'TaskController@destroy');

// Inline routes and routes grouping
Route::get('/', function () {
    return view('home');
});

Route::get('/greet/{person?}', function ($param = null) {
    $data = [
      'person'  => $param,
    ];
    return view('actions.greet', $data );
})->name('greet');

Route::group(['middleware' => 'web'], function() {
   
    Route::group(['prefix' => 'docs'], function() {
        Route::get('articles',[
           'uses'   => 'ArticlesController@topTenAction',
           'as'     => 'top.articles' 
        ])->middleware(['auth']); 
     });
});

Route::filter('https', function() {
    if ( ! Request::secure()) {
        return Redirect::secure(URI::current());
    }
});

```

## Validation Requests
```bash
php artisan make:request SaveCatRequest
```

```php
<?php
    public function rules() {
        $rules = [
        'billing_address' => 'required',
        ];
        
        if ($request->has('shipping_address_different') {
            $rules['shipping_address'] = 'required';
        }
        
        return $rules;
    }
    // ...
    
    // In the controller
    public function create(SaveCatRequest $request) {
    // method body
    }
    public function update(SaveCatRequest $request) {
    // method body
    }
```



# Model Eloquent

## Model data methods
```php
<?php
    Model::all();
    Model::find(1);
    Model::count();
    Model::max('amt');
    Model::min('amt');
    Model::avg('amt');
    Model::sum('amt');
    Model::where('id', '=', 1)->first();
    Model::where('ratings', '>', 4)->get();
    // SELECT * FROM USER WHERE gender = 'F' LIMIT 10,5
    Model::where('gender', '=', 'F')->take(5)->skip(10)->get();
    Model::withTrashed()->get();
    Model::onlyTrashed()->get();
    Model::destroy(1);
    Model::destroy(1,2,4,5);
    $model->restore();
    $model->forceDelete();
```
## Soft Deleting
Make use of *Illuminate\Database\Eloquent\SoftDeletes* traits
```php
<?php
    use Illuminate\Database\Eloquent\SoftDeletes;
    class Cat extends Model {
        use SoftDeletes;
        protected $dates = ['deleted_at'];
    }
    
    // make sure to use softDelete method in the migration script
    public function up() {
        $table->softDeletes();
    }
```
## Scoped Function For Re-usability (Query Scopes)
Convention over configuration make use of prefix scope for method names
```php
<?php
class User extends Model {
    public function scopeOver21($query)
    {
        $date = Carbon::now()->subYears(21);
        return $query->where('birth_date', '<', $date);
    }
}
$usersOver21 = User::over21()->get();
```

## Relationships
1. One-to-one
1. Many-to-many
1. Has-many-through
1. Polymorphic relations
1. Many-to-many polymorphic relations
### One-to-One
```php
<?php
    class User extends Model {
        public function profile()
        {
            return $this->hasOne('App\Profile');
        }
    }
    
    class Profile extends Model {
        public function user()
        {
            return $this->belongsTo('App\User');
        }
    }
    // Example
    $profile = User::find(1)->profile;
```

### Many-to-many
```php
<?php
    class User extends Model {
        public function roles()
        {
            return $this->belongsToMany('App\Role');
        }
    }
    
    class Role extends Model {
        public function users()
        {
            return $this->belongsToMany('App\User');
        }
    }
    
    // Example
    $roles = User::find(1)->roles;
    $admins = Role::find(1)->users;
    // new role
    $user = User::find(1);
    $user->roles()->attach($roleId);
    // removing role
    $user->roles()->detach($roleId);
    /*
     The difference with sync is, only after
     the operation is complete are the IDs that are passed present in the join table, rather
     than adding/removing them from the existing relations.
    */
    $user->roles()->sync(1, 2, 3, 4, 5);
    
    // Storing data in pivot table (junction table)
    $user->groups()->attach(1, ['is_moderator' => true]);
    $user->groups()->sync([1 => ['is_moderator' => true]]);
```
### Has-many-through
```php
<?php
    class Product extends Model {
        public function orders()
        {
        return $this->hasManyThrough('App\Order', 'App\OrderItem');
        }
    }
    // example
    $product = Product::find(1);
    $orders = $product->orders;
```

### Polymorphic relations
```php
<?php
    class Image extends Model {
        public function imageable()
        {
            return $this->morphTo();
        }
    }
    
    class Article extends Model {
        public function images()
        {
            $this->morphMany('App\Image', 'imageable');
        }
    }
    // in the migration script
    // creates image_id and image_type in the underlying db table
    $table->morphs('imageable');
```

### Many-to-many polymorphic relations

```php
<?php
    class Article extends Model {
        public function images()
        {
            return $this->morphedByMany('App\Image', 'imageable');
        }
    }
    
    class Image extends Model {
        public function articles()
        {
            return $this->morphToMany('App\Article', 'imageable');
        }
        
        public function products()
        {
            return $this->morphToMany('App\Product', 'imageable');
        }
        // And any other relations
    }
```
## Model Events
1. creating
1. created
1. updating
1. updated
1. saving
1. saved
1. deleting
1. deleted
1. resotring
1. restored

## Registering event listeners
One place to register event is int *boot* method within *EventServiceProvider* class

```php
<?php
use Illuminate\Contracts\Bus\Dispatcher as DispatcherContract;
    // ...
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        User::creating(function($user)
        {
            // Do something
        });
    }
    // ...
```

Creating slugs for SEO friendly URLs
```php
<?php
   Article::saving(function($article)
   {
        $article->slug = Str::slug($article->headline);
   }); 
```

## Model Observers
A way to reduce the over bloating of *EventServiceProvider* 
```php
<?php
    use Illuminate\Support\Str;
    class ArticleObserver {
        public function saving($article)
        {
            $article->slug = Str::slug($article->headline);
        }
    }
    
    // Inside the EventServiceProvider boot method
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        Article::observe(new ArticleObserver);
    }
```

## Collections
### Built in Collections Example
```php
<?php
    $users = User::all();
    if ($users->contains($userId))
    {
        // Do something
    }
    
    $user = User::find(1);
    if ($user->roles->contains($roleId))
    {
        // Do something
    }
```

### Custom Collections
```php
<?php 

    use Illuminate\Database\Eloquent\Collection as EloquentCollection;

    class RoleCollection extends EloquentCollection {
        public function containsAdmin()
        {
            return $this->contains(1);
        }
    }
```

Tell the Role model about the collection
```php
<?php
    class Role extends Model {
        public function newCollection(array $models = array())
        {
            return new RoleCollection($models);
        }
    }
    
    // usage of custom collection
    
    $user = User::find(1);
    if ($user->roles->containsAdmin())
    {
        // Let user administrate something
    }
    else
    {
        // User does not have administrator role
    }
```
# Policies
 
 In your app/Providers/RouteServiceProvider.php file's boot method, let's 
 add the following line of code:
```php
<?php
 $router->model('task', 'App\Task');
```
 
 Finally, we need to associate our Task model with our TaskPolicy. 
 We can do this by adding a line in the app/Providers/AuthServiceProvider.php file's 
 $policies property. This will inform Laravel which policy should be used 
 whenever we try to authorize an action on a Task instance:
 
```php
<?php
 /**
 * The policy mappings for the application.
 *
 * @var array
 */
protected $policies = [
    Task::class => TaskPolicy::class,
];
```
