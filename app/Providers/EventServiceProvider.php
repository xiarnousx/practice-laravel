<?php

namespace App\Providers;

use App\Entities\Author;
use App\Entities\Quote;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\QuoteCreated' => [
            'App\Listeners\CreateLogEntry',
            'App\Listeners\SendUserNotification',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        /*Event::listen('App\Events\QuoteCreated',
           [ 'App\Listeners\CreateLogEntry', 'App\Listeners\SendUserNotification' ]);*/
    }
}
