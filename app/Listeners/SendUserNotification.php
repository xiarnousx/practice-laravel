<?php

namespace App\Listeners;

use App\Events\QuoteCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendUserNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteCreated  $event
     * @return void
     */
    public function handle(QuoteCreated $event)
    {
        $author = $event->author;

        Mail::send (
            'email.notify',
            [ 'name' => $author->name ],
            function($message) use ($author) {
                $message->from('admin@admin.com', 'Admin');
                $message->to($author->email, $author->name);
                $message->subject('Well said!');
            }
        );
    }
}
