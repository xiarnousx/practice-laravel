<?php

namespace App\Http\Controllers;

use App\Entities\Author;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\LoginFormRequest;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function getLogin()
    {
        return view('admin.login');
    }

    public function getDashboard()
    {

      /*
       * // Better approach using middleware before interceptor
         if (!Auth::check()) {
              return redirect()->back();
        }
      */

        $data = [
            'authors' => Author::all(),
        ];

        return view('admin.dashboard', $data);
    }

    public function postLogin(LoginFormRequest $r)
    {

        if (!Auth::attempt(['name'=> $r['name'], 'password' => $r['password']])) {
            return redirect()->back()->with(['fail' => 'Ooops forgot your credentials']);
        }

        return redirect()->route('admin.dashboard');
    }

    public function logoutAction()
    {
        Auth::logout();

        return redirect()->route('index');
    }
}
