<?php

namespace App\Http\Controllers;


use App\Entities\AuthorLog;
use App\Events\QuoteCreated;
use Illuminate\Support\Facades\Event;
use App\Entities\Quote;
use App\Entities\Author;
use App\Http\Requests\QuoteRequest;

class QuoteController extends Controller
{
    public function getIndexAction($author = null)
    {
        $eAuthor = null;
        $quotes = [];

        if (! is_null($author)) {
            $eAuthor = Author::where('name', '=', $author)->first();
            if ($eAuthor) {
                $quotes = $eAuthor->quotes()->orderBy('created_at', 'desc')->paginate(6);
            }
        } else {
            $quotes = Quote::orderBy('created_at', 'desc')->paginate(6);
        }

        $data = [
          'quotes'   => $quotes,
        ];
        return view('home', $data);
    }

    public function postQuoteAction(QuoteRequest $r)
    {
        $sAuthor = ucfirst($r['author']);
        $sQuote = $r['quote'];
        $sEmail = $r['email'];

        $eAuthor = Author::where('name', $sAuthor)->first();

        if (!$eAuthor) {
            $eAuthor = new Author();
            $eAuthor->name = $sAuthor;
            $eAuthor->email = $sEmail;
            $eAuthor->save();
        }

        $eQuote = new Quote();
        $eQuote->quote = $sQuote;
        $eAuthor->quotes()->save($eQuote);

        // fire the event
        Event::fire(new QuoteCreated($eAuthor));
        $data = [
          'success' => 'Quote Saved!',
        ];
        return redirect()->back()->with($data);
    }

    public function deleteQuoteAction($quoteId)
    {
        $isAuthorDeleted = false;
        $eQuote = Quote::find($quoteId);
        if ($eQuote) {
            if ( count($eQuote->author->quotes) === 1) {
                $eQuote->author->delete();
                $isAuthorDeleted = true;
            }

            $eQuote->delete();
        }

        $data = [
          'success' => $isAuthorDeleted ? 'Quote and Author deleted!' : 'Quote deleted',
        ];
        return redirect()->back()->with($data);
    }

    public function mailCallback($author)
    {

        $eAuthorLog = new AuthorLog();
        $eAuthorLog->author = $author;
        $eAuthorLog->save();

        return view('email.notify', ['name' => $author]);
    }
}
