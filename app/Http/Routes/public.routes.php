<?php
/**
 * Created by PhpStorm.
 * User: xiarnousx
 * Date: 6/24/16
 * Time: 11:16 PM
 */

Route::group(['prefix' => 'app'], function() {
    Route::get('/{author?}',[
        'uses'  => 'QuoteController@getIndexAction',
        'as'    => 'index',
    ]);

    Route::post('/new', [
        'uses'  => 'QuoteController@postQuoteAction',
        'as'    => 'create',
    ]);

    Route::get('/delete/{quoteId}', [
        'uses'   => 'QuoteController@deleteQuoteAction',
        'as'    => 'delete',
    ]);

    Route::get('/you-ve-got-mail/{author}',[
        'uses'   => 'QuoteController@mailCallback',
        'as'     => 'mail.callback'
    ]);
});