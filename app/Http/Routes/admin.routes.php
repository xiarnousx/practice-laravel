<?php
/**
 * Created by PhpStorm.
 * User: xiarnousx
 * Date: 6/24/16
 * Time: 11:16 PM
 */

Route::group(['prefix' => 'admin'], function(){

    Route::get('/', [
        'uses'   => 'AdminController@getLogin',
        'as'     => 'admin.login'
    ]);

    Route::post('/', [
        'uses'   => 'AdminController@postLogin',
        'as'     => 'admin.login'
    ]);

    Route::get('/dashboard', [
        'uses'          => 'AdminController@getDashboard',
        'middleware'    => ['auth'],
        'as'            => 'admin.dashboard'
    ]);

    Route::get('/logout',[
        'uses'  => 'AdminController@logoutAction',
        'as'    => 'admin.logout',
    ]);
});