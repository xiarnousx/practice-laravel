<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class QuoteLog extends Model
{
    protected $table = 'quote_log';
}
