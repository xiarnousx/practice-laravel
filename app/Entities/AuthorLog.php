<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AuthorLog extends Model
{
    protected $table = 'author_log';
}
