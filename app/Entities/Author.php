<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function quotes()
    {
        return $this->hasMany('App\Entities\Quote');
    }
}
