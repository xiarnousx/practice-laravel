<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Entities\Author');
    }
}
