@extends('layouts.master')
@section('title')
    Trending Quotes
@endsection
@section('styles')
    <link href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css"
            type="text/css" rel="stylesheet" />
@endsection
@section('content')
    @if(!empty(Request::segment(2)))
        <section class="filter-bar">
            A filter has been set! <a href="{{ route('index') }}">Show all quotes</a>
        </section>
        @endif
    @if (Session::has('success'))
        <section class="info-box success">
            {{ Session::get('success') }}
        </section>
        @endif
    <section class="quotes">
        <h1>Latest Quote</h1>
        @for ($i = 0; $i < count($quotes); $i++)
            <article class="quote">
                <div class="delete"><a href="{{ route('delete', ['quoteId' => $quotes[$i]->id]) }}">x</a></div>
                {{ $quotes[$i]->quote }}
                <div class="info">Created By <a href="{{ route('index', ['author' => $quotes[$i]->author->name]) }}">{{ $quotes[$i]->author->name }}</a> on {{ $quotes[$i]->created_at }}</div>
            </article>

        @endfor

        <div class="pagination">
            @if($quotes->currentPage() !== 1)
                <a href="{{ $quotes->previousPageUrl() }}"><span class="glyphicon-arrow-left">&lt;</span></a>
            @endif
            @if($quotes->hasPages() && $quotes->currentPage() !== $quotes->lastPage())
                <a href="{{ $quotes->nextPageUrl() }}"><span class="glyphicon-arrow-right">&gt;</span></a>
                @endif
        </div>
    </section>
    @include('includes.validation')
    <section class="edit-quote">
        <h1>Add a Quote</h1>
        <form method="post" action="{{ route('create') }}">
            <div class="input-group">
                <label for="author"></label>
                <input type="text" id="author" name="author" placeholder="Enter your name" />
            </div>
            <div class="input-group">
                <label for="email"></label>
                <input type="email" id="email" name="email" placeholder="Enter your email" />
            </div>
            <div class="input-group">
                <label for="quote"></label>
                <textarea type="text" id="quote" name="quote" placeholder="Enter your quote" rows="5" ></textarea>
            </div>
            {!!   csrf_field() !!}
            <button type="submit" class="btn">Say</button>
        </form>
    </section>
@endsection
