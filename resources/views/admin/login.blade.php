@extends('layouts.master')

@section('title')
    Admin | Login
    @endsection

@section('styles')
    <style>
    .input-group label {
        display: inline-block;
    }
    </style>
    @endsection
@section('content')
    @if(Session::has('fail'))
        <section class="info-box fail">
            {{ Session::get('fail') }}
        </section>
        @endif

    @include('includes.validation')

    <form method="post" action="{{ route('admin.login') }}">
        <div class="input-group">
            <label for="name">Username :</label>
            <input type="text" id="name" name="name" placeholder="Enter your name" />
        </div>
        <div class="input-group">
            <label for="password">Password: </label>
            <input type="password" id="password" name="password" placeholder="Enter your password" />
        </div>
        {!!   csrf_field() !!}
        <button type="submit" class="btn">Login</button>
    </form>
    @endsection