@extends('layouts.master')

@section('title')
    secure area
    @endsection
@section('content')
    <ul>
    @foreach($authors as $author)
        <li>{{$author->name}} |  {{ $author->email }}</li>
        @endforeach
    </ul>
    @endsection