@if (count($errors) > 0)
    <section class="info-box fail">
        @foreach($errors->all() as $err)
            {{ $err }}
        @endforeach
    </section>
@endif